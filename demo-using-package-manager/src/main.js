import Vue from 'vue'
import App from './App.vue'
import 'tsp-components-library-demo/dist/tsp.common'
import 'tsp-components-library-demo/dist/tsp.css'
Vue.config.productionTip = false

new Vue({
  render: h => h(App),
}).$mount('#app')
