import TspButton from "@/components/TspButton";
import TspBadge from "@/components/TspBadge";
import TspAlert from "@/components/TspAlert";
import Vue from 'vue';

// export const components = {
//     TspButton,
//     TspBadge,
//     TspAlert
// };

// var TspBadge = tsp.components.TspBadge;
// var TspButton = tsp.components.TspButton;
// var TspAlert = tsp.components.TspAlert;

Vue.component('tsp-badge', TspBadge);
Vue.component('tsp-button', TspButton);
Vue.component('tsp-alert', TspAlert);